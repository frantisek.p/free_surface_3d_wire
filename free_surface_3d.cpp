/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2021 Claudius Holeksa
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#include "olb3D.h"
#include "olb3D.hh"

#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <memory>
#include <fstream>
#include <sstream>
#include <map>

#include <fenv.h>

using namespace olb;
using namespace olb::descriptors;

using T = double;
using DESCRIPTOR = D3Q27<descriptors::FORCE, FreeSurface::EPSILON, FreeSurface::MASS, FreeSurface::CELL_TYPE, FreeSurface::CELL_FLAGS, FreeSurface::TEMP_MASS_EXCHANGE, FreeSurface::PREVIOUS_VELOCITY>;

struct FreeSurface3DAppHelper {
  std::array<T,3> area;
  std::function<void(UnitConverter<T,DESCRIPTOR> const&, SuperLattice<T, DESCRIPTOR>&, Dynamics<T, DESCRIPTOR>&, SuperGeometry3D<T>&, T, const FreeSurface3DAppHelper&)> preparation;
  std::array<T,3> gravity_force = {{0., 0., -9.81}};

  T char_phys_length = 1.;
  T char_phys_vel = 0.1;
  bool has_surface_tension = false;
  T surface_tension_coefficient = 0.0;
};

struct FallingDrop3DHelper {
  std::array<T,3> initial_falling_speed{0., 0., -6.03};
} falling_drop_helper;

template <typename T, typename DESCRIPTOR>
class FreeSurfaceSurfaceTension3D final : public AnalyticalF<DESCRIPTOR::d,T,T> {
private:
  T lattice_size;
  std::array<T, 3> cell_values;
  std::array<T, DESCRIPTOR::d> area;
  bool interpolate;
public:
  FreeSurfaceSurfaceTension3D(T lattice_size_, const std::array<T,3>& cell_vals, const std::array<T,DESCRIPTOR::d>& area_, bool interpol_):AnalyticalF<DESCRIPTOR::d,T,T>{1}, lattice_size{lattice_size_}, cell_values{cell_vals}, area{area_}, interpolate{interpol_}{}

  bool operator()(T output[], const T x[]) override {
    output[0] = cell_values[0];

    std::array<T,DESCRIPTOR::d> pos{
      x[0],
      x[1],
      x[2]
    };

    T middle = area[1] * 0.5;

    std::array<T,DESCRIPTOR::d-1> diff {
      x[1] - middle,
      x[2] - middle
    };

    T length_2 = diff[0]*diff[0] + diff[1]*diff[1];

    T number = 7.;
    T rad = area[0] / (number * 9.);
    T frequency = 2. * 3.141529 / (rad*9.);

    T rad_2 = rad * rad;

    if(length_2 < rad_2){
      output[0] = cell_values[2];
    }else{
      for(int i = -1; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
          for(int k = -1; k <= 1; ++k){
            std::array<T,DESCRIPTOR::d-1> s_diff{
              diff[0]+j*lattice_size,
              diff[1]+k*lattice_size
            };

            T s_len_2 = s_diff[0]*s_diff[0] + s_diff[1] * s_diff[1];

            if(s_len_2 < rad_2){
              if(interpolate){
                T inter = 0.5 + 0.45*std::sin(frequency * pos[0]);

                output[0] = inter;

                return true;
              }else{
                output[0] = cell_values[1];
                return true;
              }
            }
          }
        }
      }
    }
    return true;
  }
};

template <typename T, typename DESCRIPTOR>
class FreeSurfaceSurfaceTensionBall3D final : public AnalyticalF<DESCRIPTOR::d,T,T> {
private:
  T lattice_size;
  std::array<T, 3> cell_values;
  std::array<T, DESCRIPTOR::d> area;
public:
  FreeSurfaceSurfaceTensionBall3D(T lattice_size_, const std::array<T,3>& cell_vals, const std::array<T,DESCRIPTOR::d>& area_):AnalyticalF<DESCRIPTOR::d,T,T>{1}, lattice_size{lattice_size_}, cell_values{cell_vals}, area{area_}{}

  bool operator()(T output[], const T x[]) override {
    output[0] = cell_values[0];

    // area[1] and area[2] should be the same anyway
    T middle = (area[1]) * 0.5;

    std::array<T,DESCRIPTOR::d> diff{
      x[0] - middle,
      x[1] - middle,
      x[2] - middle
    };

    T length_2 = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];

    T rad = middle * 0.4;
    T rad_2 = rad*rad;

    if(length_2 < rad_2){
      output[0] = cell_values[2];
    }else{
      for(int i = -1; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
          for(int k = -1; k <= 1; ++k){

            std::array<T,DESCRIPTOR::d> diff_shift{
              diff[0] + i * lattice_size,
              diff[1] + j * lattice_size,
              diff[2] + k * lattice_size
            };

            T shift_len = diff_shift[0]*diff_shift[0] + diff_shift[1]* diff_shift[1] + diff_shift[2]*diff_shift[2];

            if(shift_len < rad_2){
              output[0] = cell_values[1];
              return true;
            }
          }
        }
      }
    }
    return true;
  }
};

template <typename T, typename DESCRIPTOR>
class FreeSurfaceSurfaceTensionCuboid3D final : public AnalyticalF<DESCRIPTOR::d,T,T> {
private:
  T lattice_size;
  std::array<T, 3> cell_values;
  std::array<T, DESCRIPTOR::d> area;
public:
  FreeSurfaceSurfaceTensionCuboid3D(T lattice_size_, const std::array<T,3>& cell_vals, const std::array<T,DESCRIPTOR::d>& area_):AnalyticalF<DESCRIPTOR::d,T,T>{1}, lattice_size{lattice_size_}, cell_values{cell_vals}, area{area_}{}

  bool operator()(T output[], const T x[]) override {
    output[0] = cell_values[0];

    // area[1] and area[2] should be the same anyway
    std::array<T,DESCRIPTOR::d> middle{
      0.5 * area[0],
      0.5 * area[1],
      0.5 * area[2]
    };

    std::array<T,DESCRIPTOR::d> diff{
      x[0] - middle[0],
      x[1] - middle[1],
      x[2] - middle[2]
    };

    std::array<T,DESCRIPTOR::d> range {
      middle[0] * 0.4,
      middle[1] * 0.4,
      middle[2] * 0.4
    };

    if(std::abs(diff[0]) < range[0] && std::abs(diff[1]) < range[1] && std::abs(diff[2]) < range[2]){
      output[0] = cell_values[2];
    }else{
      for(int i = -1; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
          for(int k = -1; k <= 1; ++k){

            std::array<T,DESCRIPTOR::d> diff_shift{
              diff[0] + i * lattice_size,
              diff[1] + j * lattice_size,
              diff[2] + k * lattice_size
            };

            if(std::abs(diff_shift[0]) < range[0] && std::abs(diff_shift[1]) < range[1] && std::abs(diff_shift[2]) < range[2]){
              output[0] = cell_values[1];
              return true;
            }
          }
        }
      }
    }
    return true;
  }
};

template <typename T, typename DESCRIPTOR>
class FreeSurfaceFallingDrop3D final : public AnalyticalF<DESCRIPTOR::d,T,T> {
private:
  T lattice_size;
  std::array<T, 3> cell_values;
public:
  FreeSurfaceFallingDrop3D(T lattice_size_, const std::array<T,3>& cell_vals):AnalyticalF<DESCRIPTOR::d,T,T>{DESCRIPTOR::d}, lattice_size{lattice_size_}, cell_values{cell_vals}{}

  bool operator()(T output[], const T x[]) override {
    output[0] = cell_values[0];
    T radius = 0.0085;

    if(x[2] <= radius){
      output[0] = cell_values[2];
    }else if(x[2] <= radius + lattice_size * 1.5){
      output[0] = cell_values[1];
    }

    std::array<T,DESCRIPTOR::d> point = {0.015, 0.015, 0.0265+radius + radius + 2 * lattice_size};
    std::array<T,DESCRIPTOR::d> diff = {x[0] - point[0], x[1] - point[1], x[2] - point[2]};

    if((diff[0]*diff[0] + diff[1] * diff[1] + diff[2] * diff[2]) <= radius*radius){
      output[0] = cell_values[2];
    }else{
      for(int i = -1; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
          for(int k = -1; k <= 1; ++k){
            std::array<T,DESCRIPTOR::d> shifted_diff = {diff[0]+i*lattice_size, diff[1]+j*lattice_size, diff[2]+ k * lattice_size};
            if((shifted_diff[0]*shifted_diff[0] + shifted_diff[1] * shifted_diff[1] + shifted_diff[2] * shifted_diff[2]) <= radius*radius){
              output[0] = cell_values[1];
              return true;
            }
          }
        }
      }
    }

    return true;
  }
};

template <typename T, typename DESCRIPTOR>
class FallingDropVel3D final : public AnalyticalF<DESCRIPTOR::d,T,T> {
private:
  T lattice_size;
  std::array<T,DESCRIPTOR::d> lattice_speed;
public:
  FallingDropVel3D(T lattice_size_, const std::array<T,DESCRIPTOR::d>& lattice_speed_):AnalyticalF<DESCRIPTOR::d,T,T>{DESCRIPTOR::d}, lattice_size{lattice_size_}, lattice_speed{lattice_speed_}{}

  bool operator()(T output[], const T x[]) override {
    T radius = 0.0085;
    std::array<T,DESCRIPTOR::d> point = {0.015, 0.015,0.0265+ radius + radius + 2 * lattice_size};
    std::array<T,DESCRIPTOR::d> diff = {x[0] - point[0], x[1] - point[1], x[2] - point[2]};

    output[0] = 0.;
    output[1] = 0.;
    output[2] = 0.;
    for(int i = -1; i <= 1; ++i){
      for(int j = -1; j <= 1; ++j){
        for(int k = -1; k <= 1; ++k){
          std::array<T,DESCRIPTOR::d> shifted_diff = {diff[0]+i*lattice_size, diff[1]+j*lattice_size, diff[2]+ k * lattice_size};
          if((shifted_diff[0]*shifted_diff[0] + shifted_diff[1] * shifted_diff[1] + shifted_diff[2] * shifted_diff[2]) <= radius*radius){
            output[0] = lattice_speed[0];
            output[1] = lattice_speed[1];
            output[2] = lattice_speed[2];
            return true;
          }
        }
      }
    }

    return true;
  }
};

void prepareGeometry( UnitConverter<T,DESCRIPTOR> const& converter,
                      SuperGeometry3D<T>& superGeometry, const FreeSurface3DAppHelper& helper ) {

  OstreamManager clout( std::cout,"prepareGeometry" );
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename( 0,2 );
  superGeometry.rename( 2,1,1,1,1 );
	  Vector<T,3> origin = {0.,0.,0};
  origin[0] = helper.area[0]*0.99;
  origin[1] = helper.area[1]/2.;
  origin[2] = helper.area[2]/2.;

  Vector<T,3> extend = {0.,0.,0};
  extend[0] = helper.area[0]*0.01;
  extend[1] = helper.area[1]/2.;
  extend[2] = helper.area[2]/2.;
  //origin[0] = superGeometry.getStatistics().getMinPhysR( 2 )[0]+converter.getConversionFactorLength();
  //extend[0] = ( superGeometry.getStatistics().getMaxPhysR( 2 )[0]-superGeometry.getStatistics().getMinPhysR( 2 )[0] )/2.;
  std::shared_ptr<IndicatorF3D<T>> cylinder = std::make_shared<IndicatorCylinder3D<T>>( extend, origin, helper.area[0]/24. );
  superGeometry.rename( 1,3, cylinder );					 

  superGeometry.clean();
  superGeometry.innerClean();
  superGeometry.checkForErrors();

  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}

void prepareFallingDrop(UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperLattice<T, DESCRIPTOR>& sLattice,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     SuperGeometry3D<T>& superGeometry, T lattice_size, const FreeSurface3DAppHelper& helper)
{
  AnalyticalConst3D<T,T> zero( 0. );
  AnalyticalConst3D<T,T> one( 1. );
  AnalyticalConst3D<T,T> two( 2. );
  AnalyticalConst3D<T,T> four( 4. );
  FreeSurfaceFallingDrop3D<T,DESCRIPTOR> cells_analytical{ lattice_size, {0., 1., 2.}};
  FreeSurfaceFallingDrop3D<T,DESCRIPTOR> mass_analytical{ lattice_size, {0., 0.5, 1.}};

  AnalyticalConst3D<T,T> force_zero{0., 0., 0.};

  for (int i: {0,1,2}) {
    sLattice.defineField<FreeSurface::MASS>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::EPSILON>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_FLAGS>(superGeometry, i, zero);
    sLattice.defineField<descriptors::FORCE>(superGeometry, i, force_zero);
  }

  sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, 1, cells_analytical);

  sLattice.defineField<FreeSurface::MASS>(superGeometry, 1, mass_analytical);
  sLattice.defineField<FreeSurface::EPSILON>(superGeometry, 1, mass_analytical);

  for (int i: {0,2}) {
    //sLattice.defineField<MASS>(superGeometry, i, one);
    //sLattice.defineField<EPSILON>(superGeometry, i, one);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, four);
  }

  T force_factor = 1./ converter.getConversionFactorForce() * converter.getConversionFactorMass();
  AnalyticalConst3D<T,T> force_a{helper.gravity_force[0] * force_factor, helper.gravity_force[1] * force_factor, helper.gravity_force[2]*force_factor};
  sLattice.defineField<descriptors::FORCE>(superGeometry.getMaterialIndicator({1}), force_a);
}

void prepareSurfaceTension(UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperLattice<T, DESCRIPTOR>& sLattice,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     SuperGeometry3D<T>& superGeometry, T lattice_size, const FreeSurface3DAppHelper& helper)
{
  AnalyticalConst3D<T,T> zero( 0. );
  AnalyticalConst3D<T,T> one( 1. );
  AnalyticalConst3D<T,T> two( 2. );
  AnalyticalConst3D<T,T> four( 4. );
  FreeSurfaceSurfaceTension3D<T,DESCRIPTOR> cells_analytical{ lattice_size, {0., 1., 2.}, helper.area, false};
  FreeSurfaceSurfaceTension3D<T,DESCRIPTOR> mass_analytical{ lattice_size, {0., 0.5, 1.}, helper.area, true};

  AnalyticalConst3D<T,T> force_zero{0., 0., 0.};

  for (int i: {0,1,2}) {
    sLattice.defineField<FreeSurface::MASS>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::EPSILON>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_FLAGS>(superGeometry, i, zero);
    sLattice.defineField<descriptors::FORCE>(superGeometry, i, force_zero);
  }

  sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, 1, cells_analytical);

  sLattice.defineField<FreeSurface::MASS>(superGeometry, 1, mass_analytical);
  sLattice.defineField<FreeSurface::EPSILON>(superGeometry, 1, mass_analytical);

  for (int i: {0,2}) {
    //sLattice.defineField<MASS>(superGeometry, i, one);
    //sLattice.defineField<EPSILON>(superGeometry, i, one);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, four);
  }

  T force_factor = 1./ converter.getConversionFactorForce() * converter.getConversionFactorMass();
  AnalyticalConst3D<T,T> force_a{helper.gravity_force[0] * force_factor, helper.gravity_force[1] * force_factor, helper.gravity_force[2]*force_factor};
  sLattice.defineField<descriptors::FORCE>(superGeometry.getMaterialIndicator({1}), force_a);
}

void prepareSurfaceTensionBall(UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperLattice<T, DESCRIPTOR>& sLattice,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     SuperGeometry3D<T>& superGeometry, T lattice_size, const FreeSurface3DAppHelper& helper)
{
  AnalyticalConst3D<T,T> zero( 0. );
  AnalyticalConst3D<T,T> one( 1. );
  AnalyticalConst3D<T,T> two( 2. );
  AnalyticalConst3D<T,T> four( 4. );
  FreeSurfaceSurfaceTensionBall3D<T,DESCRIPTOR> cells_analytical{ lattice_size, {0., 1., 2.}, helper.area};
  FreeSurfaceSurfaceTensionBall3D<T,DESCRIPTOR> mass_analytical{ lattice_size, {0., 0.5, 1.}, helper.area};

  AnalyticalConst3D<T,T> force_zero{0., 0., 0.};

  for (int i: {0,1,2}) {
    sLattice.defineField<FreeSurface::MASS>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::EPSILON>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_FLAGS>(superGeometry, i, zero);
    sLattice.defineField<descriptors::FORCE>(superGeometry, i, force_zero);
  }

  sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, 1, cells_analytical);

  sLattice.defineField<FreeSurface::MASS>(superGeometry, 1, mass_analytical);
  sLattice.defineField<FreeSurface::EPSILON>(superGeometry, 1, mass_analytical);

  for (int i: {0,2}) {
    //sLattice.defineField<MASS>(superGeometry, i, one);
    //sLattice.defineField<EPSILON>(superGeometry, i, one);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, four);
  }

  T force_factor = 1./ converter.getConversionFactorForce() * converter.getConversionFactorMass();
  AnalyticalConst3D<T,T> force_a{helper.gravity_force[0] * force_factor, helper.gravity_force[1] * force_factor, helper.gravity_force[2]*force_factor};
  sLattice.defineField<descriptors::FORCE>(superGeometry.getMaterialIndicator({1}), force_a);
}

void prepareSurfaceTensionCuboid(UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperLattice<T, DESCRIPTOR>& sLattice,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     SuperGeometry3D<T>& superGeometry, T lattice_size, const FreeSurface3DAppHelper& helper)
{
  AnalyticalConst3D<T,T> zero( 0. );
  AnalyticalConst3D<T,T> one( 1. );
  AnalyticalConst3D<T,T> two( 2. );
  AnalyticalConst3D<T,T> four( 4. );
  FreeSurfaceSurfaceTensionCuboid3D<T,DESCRIPTOR> cells_analytical{ lattice_size, {0., 1., 2.}, helper.area};
  FreeSurfaceSurfaceTensionCuboid3D<T,DESCRIPTOR> mass_analytical{ lattice_size, {0., 0.5, 1.}, helper.area};

  AnalyticalConst3D<T,T> force_zero{0., 0., 0.};

  for (int i: {0,1,2}) {
    sLattice.defineField<FreeSurface::MASS>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::EPSILON>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, zero);
    sLattice.defineField<FreeSurface::CELL_FLAGS>(superGeometry, i, zero);
    sLattice.defineField<descriptors::FORCE>(superGeometry, i, force_zero);
  }

  sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, 1, cells_analytical);

  sLattice.defineField<FreeSurface::MASS>(superGeometry, 1, mass_analytical);
  sLattice.defineField<FreeSurface::EPSILON>(superGeometry, 1, mass_analytical);

  for (int i: {0,2}) {
    //sLattice.defineField<MASS>(superGeometry, i, one);
    //sLattice.defineField<EPSILON>(superGeometry, i, one);
    sLattice.defineField<FreeSurface::CELL_TYPE>(superGeometry, i, four);
  }

  T force_factor = 1./ converter.getConversionFactorForce() * converter.getConversionFactorMass();
  AnalyticalConst3D<T,T> force_a{helper.gravity_force[0] * force_factor, helper.gravity_force[1] * force_factor, helper.gravity_force[2]*force_factor};
  sLattice.defineField<descriptors::FORCE>(superGeometry.getMaterialIndicator({1}), force_a);
}


std::map<std::string, FreeSurface3DAppHelper> free_surface_3d_configs {
  {"fallingDrop", 
    {
      {0.03, 0.03, 0.06},
      prepareFallingDrop,
      {0.,0.,-9.81},
      0.03,
      6.03,
      true,
      0.0663
    }
  },
  {"surfaceTension",
    {
      {512, 50, 50},
      prepareSurfaceTension,
      {0.,0.,0.},
      512,
      0.1,
      true,
      0.1
    }
  },
  {"surfaceTensionBall",
    {
      {1., 1., 1.},
      prepareSurfaceTensionBall,
      {0.,0.,0.},
      1.,
      1.0,
      true,
      0.01
    }
  },
  {"surfaceTensionCuboid",
    {
      {1., 1., 1.},
      prepareSurfaceTensionCuboid,
      {0.,0.,0.},
      1.,
      1.0,
      true,
      0.1
    }
  }/*,
  {"breakingDam",
    {
      {1.,1., 1.},
      prepareBreakingDam,
      {0.,-9.81},
      1.,
      1.0
    }
  }
  */
};

void prepareLattice( UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperLattice<T, DESCRIPTOR>& sLattice,
                     Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     SuperGeometry3D<T>& superGeometry, const std::string& model, T lattice_size, const FreeSurface3DAppHelper& helper ) {

  OstreamManager clout( std::cout,"prepareLattice" );
  clout << "Prepare Lattice ..." << std::endl;

  // Material=0 -->do nothing
  sLattice.defineDynamics( superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>() );
  // Material=1 -->bulk dynamics
  sLattice.defineDynamics( superGeometry, 1, &bulkDynamics );
  // Material=2 -->no-slip boundary
  // sLattice.defineDynamics( superGeometry, 2, &instances::getNoDynamics<T, DESCRIPTOR>() );
  setSlipBoundary<T,DESCRIPTOR>(sLattice, superGeometry, 2);
	setSlipBoundary<T,DESCRIPTOR>(sLattice, superGeometry, 3);														  

  //AnalyticalConst2D<T,T> force(0.0, 1.e-10);
  //sLattice.defineField<FORCE>(superGeometry, 1, force);

  helper.preparation(converter, sLattice, bulkDynamics, superGeometry, lattice_size, helper);
  clout << "Prepare Lattice ... OK" << std::endl;
}

void setInitialValues(SuperLattice<T, DESCRIPTOR>& sLattice, SuperGeometry3D<T>& sGeometry, const std::string& model, T lattice_length, UnitConverter<T,DESCRIPTOR> const& converter){
  
  if(model == "fallingDrop"){
    std::array<T,3> lattice_speed{
      falling_drop_helper.initial_falling_speed[0] * converter.getPhysDeltaT() / converter.getPhysDeltaX(),
      falling_drop_helper.initial_falling_speed[1] * converter.getPhysDeltaT() / converter.getPhysDeltaX(),
      falling_drop_helper.initial_falling_speed[2] * converter.getPhysDeltaT() / converter.getPhysDeltaX()
    };

    FallingDropVel3D<T,DESCRIPTOR> u{lattice_length, lattice_speed};
    AnalyticalConst3D<T,T> one(1.);
    sLattice.defineRhoU( sGeometry.getMaterialIndicator({0,1,2}), one, u );
    for (int i: {0,1,2}) {
      sLattice.iniEquilibrium( sGeometry, i, one, u );
    }
  }else{
    AnalyticalConst3D<T,T> u{0., 0.};
    AnalyticalConst3D<T,T> one(1.);

    sLattice.defineRhoU( sGeometry.getMaterialIndicator({0,1,2}), one, u );
    for (int i: {0,1,2}) {
      sLattice.iniEquilibrium( sGeometry, i, one, u );
    }
  }

  // Set up free surface communicator stages
  FreeSurface::initialize(sLattice);
  // Make the lattice ready for simulation
  sLattice.initialize();
}

void getResults( SuperLattice<T,DESCRIPTOR>& sLattice, Dynamics<T, DESCRIPTOR>& bulkDynamics,
                 UnitConverter<T,DESCRIPTOR> const& converter, int iT,
                 SuperGeometry3D<T>& superGeometry, Timer<T>& timer )
{

  OstreamManager clout( std::cout,"getResults" );

  SuperVTMwriter3D<T> vtmWriter( "freeSurface" );
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity( sLattice, converter );
  SuperLatticePhysPressure3D<T, DESCRIPTOR> pressure( sLattice, converter );
  SuperLatticeExternalScalarField3D<T, DESCRIPTOR, FreeSurface::EPSILON> epsilon( sLattice );
  SuperLatticeExternalScalarField3D<T, DESCRIPTOR, FreeSurface::CELL_TYPE> cells( sLattice );
  SuperLatticeExternalScalarField3D<T, DESCRIPTOR, FreeSurface::CELL_TYPE> flags( sLattice );
  SuperLatticeExternalScalarField3D<T, DESCRIPTOR, FreeSurface::MASS> mass( sLattice );
  SuperLatticeGeometry3D<T, DESCRIPTOR> geometry(sLattice, superGeometry);
  epsilon.getName() = "epsilon";
  cells.getName() = "cell_type";
  flags.getName() = "cell_flags";
  mass.getName() = "mass";
  vtmWriter.addFunctor( velocity );
  vtmWriter.addFunctor( pressure );
  vtmWriter.addFunctor( epsilon );
  vtmWriter.addFunctor( cells );
  //vtmWriter.addFunctor( flags );
  vtmWriter.addFunctor( mass );
  vtmWriter.addFunctor( geometry );

  const int vtmIter  = 100;//converter.getLatticeTime( maxPhysT/2000. );
  const int statIter = 100;//converter.getLatticeTime( maxPhysT/2000. );

  if ( iT==0 ) {
    // Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry3D<T, DESCRIPTOR> geometry( sLattice, superGeometry );
    SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid( sLattice );
    SuperLatticeRank3D<T, DESCRIPTOR> rank( sLattice );
    vtmWriter.write( geometry );
    vtmWriter.write( cuboid );
    vtmWriter.write( rank );

    vtmWriter.createMasterFile();
  }

  // Writes the vtm files and profile text file
  if ( iT%vtmIter==0 ) {
    //comms.mass.communicate();
    //comms.epsilon.communicate();
    //comms.cell_type.communicate();
    vtmWriter.write( iT );
  }

  // Writes output on the console
  if ( iT%statIter==0 ) {
    // Timer console output
    timer.update( iT );
    timer.printStep();

    // Lattice statistics console output
    sLattice.getStatistics().print( iT,converter.getPhysTime( iT ) );
  }
}

namespace {

class FreeSurfaceConfig {
public:
  std::string model = "fallingDrop";
  T viscosity = 0.000001;
  T density = 1.;
  T physTime = 1.;
  T latticeRelaxationTime = .5001;
  int N = 100;

  T transitionThreshold = 1e-3;
  T lonelyThreshold = 0.1;

  std::string generateString() const {
    std::ostringstream oss;

    oss<<"./tmp/"<<model<<"_"<<std::to_string(viscosity)<<"_"<<std::to_string(density)<<"_"<<std::to_string(physTime)<<"_"<<std::to_string(latticeRelaxationTime)<<"_"<<std::to_string(N)<<"/";

    return oss.str();
  }
};

FreeSurfaceConfig parseArgs(int argc, char** argv){
  FreeSurfaceConfig config;

  int i = 1;
  if(argc >= (i+1)){
    config.model = std::string{argv[i]};
    ++i;
  }

  if(argc >= (i+1)){
    config.viscosity = std::stod(std::string{argv[i]});
    ++i;
  }
  if(argc >= (i+1)){
    config.density = std::stod(std::string{argv[i]});
    ++i;
  }
  if(argc >= (i+1)){
    config.physTime = std::stod(std::string{argv[i]});
    ++i;
  }
  if(argc >= (i+1)){
    config.latticeRelaxationTime = std::stod(std::string{argv[i]});
    ++i;
  }

  if(argc >= (i+1)){
    config.N = std::stoi(std::string{argv[i]});
    ++i;
  }

  return config;
}
}

int main(int argc, char** argv){

	olbInit(&argc, &argv, false, false);

  FreeSurfaceConfig c = parseArgs(argc, argv);

  OstreamManager clout (std::cout, "main");
  OstreamManager clerr( std::cerr, "main" );

  auto find_setup_config = free_surface_3d_configs.find(c.model);
  if(find_setup_config == free_surface_3d_configs.end()){
    clerr<<"Couldn't find free surface model"<<std::endl;
    return -1;
  }
  singleton::directories().setOutputDir(c.generateString());

  FreeSurface3DAppHelper& helper = find_setup_config->second;

  UnitConverterFromResolutionAndRelaxationTime<T, DESCRIPTOR> const converter(
    int {c.N},     // resolution: number of voxels per charPhysL
    (T)   c.latticeRelaxationTime,   // latticeRelaxationTime: relaxation time, have to be greater than 0.5!
    (T)   helper.char_phys_length,     // charPhysLength: reference length of simulation geometry
    (T)   helper.char_phys_vel,     // charPhysVelocity: maximal/highest expected velocity during simulation in __m / s__
    (T)   c.viscosity, // physViscosity: physical kinematic viscosity in __m^2 / s__
    (T)   c.density     // physDensity: physical density in __kg / m^3__
  );

  // Prints the converter log as console output
  converter.print();
  // Writes the converter log in a file
  converter.write("free surface");

  T lattice_size = helper.char_phys_length / c.N;
  T force_conversion_factor = 1./converter.getConversionFactorForce()*converter.getConversionFactorMass();
  T surface_tension_coefficient_factor = std::pow(converter.getConversionFactorTime(),2)/ (c.density * std::pow(converter.getConversionFactorLength(),3));
  clout<<"Surface: "<<surface_tension_coefficient_factor * helper.surface_tension_coefficient<<std::endl;

  // === 2nd Step: Prepare Geometry ===
  Vector<T,3> extend( helper.area[0], helper.area[1], helper.area[2] );
  Vector<T,3> origin;
  IndicatorCuboid3D<T> cuboid( extend, origin );

  // Instantiation of a cuboidGeometry with weights
#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = singleton::mpi().getSize();
#else
  const int noOfCuboids = 4;
#endif
  CuboidGeometry3D<T> cuboidGeometry( cuboid, converter.getConversionFactorLength(), noOfCuboids );

  if(c.model == "surfaceTension"){
    cuboidGeometry.setPeriodicity(true, false, false);
  }

  HeuristicLoadBalancer<T> loadBalancer( cuboidGeometry );
  SuperGeometry3D<T> superGeometry( cuboidGeometry, loadBalancer, 2 );

  prepareGeometry( converter, superGeometry, helper );

  SuperLattice<T, DESCRIPTOR> sLattice( superGeometry );

  // ForcedBGKdynamics<T, DESCRIPTOR> bulkDynamics( converter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T, DESCRIPTOR>() );
  // BGKdynamics<T, DESCRIPTOR> bulkDynamics( converter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T, DESCRIPTOR>() );
  // SmagorinskyBGKdynamics<T,DESCRIPTOR> bulkDynamics(converter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T, DESCRIPTOR>(), 0.1);
  SmagorinskyForcedBGKdynamics<T,DESCRIPTOR> bulkDynamics(converter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T, DESCRIPTOR>(), 0.2);

  prepareLattice( converter, sLattice, bulkDynamics, superGeometry, c.model, lattice_size, helper );

  /*
  * @param 0 Force acting on the fluid
  * @param 1 Communicator struct
  * @param 2 Transition of cells
  * @param 3 Lonelyness threshold
  */
  FreeSurface3DSetup<T,DESCRIPTOR> free_surface_setup{sLattice,
    FreeSurface3D::Variables<T,DESCRIPTOR>{true, c.transitionThreshold, c.lonelyThreshold, helper.has_surface_tension, surface_tension_coefficient_factor * helper.surface_tension_coefficient, force_conversion_factor}
  };

  free_surface_setup.addPostProcessor();

  // === 4th Step: Main Loop with Timer ===
  clout << "starting simulation..." << endl;
  Timer<T> timer( converter.getLatticeTime( c.physTime ), superGeometry.getStatistics().getNvoxel() );
  timer.start();

  setInitialValues(sLattice, superGeometry, c.model, lattice_size, converter);

  //force_field.communicate();

  for ( std::size_t iT = 0; iT < converter.getLatticeTime( c.physTime ); ++iT ) {
    getResults( sLattice, bulkDynamics, converter, iT, superGeometry, timer );
    sLattice.collideAndStream();
  }

  timer.stop();
  timer.printSummary();

  return 0;
}
